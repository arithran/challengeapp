DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR


find app/tmp/cache/. -type f -exec rm -f {} \;
echo 'Clear Cache Done';
