-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: 52.56.203.193    Database: challenge_app_test
-- ------------------------------------------------------
-- Server version	5.6.34-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `text` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,1,NULL,'Bob','Nice Article!','2017-07-09 17:37:20','2017-07-09 17:37:20'),(2,1,1,'Ari','Thanks Bob!','2017-07-09 17:37:30','2017-07-09 17:37:30'),(3,1,NULL,'Bill','It\'s not bad','2017-07-09 17:37:51','2017-07-09 17:37:51'),(4,2,NULL,'Bob','I like this article better than the first!','2017-07-09 17:38:35','2017-07-09 17:38:35');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `body` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'This is a test page','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at posuere dui. Donec magna purus, posuere vitae egestas id, eleifend at neque. Cras egestas, diam eu dictum volutpat, velit orci tincidunt lacus, sit amet pretium mi magna ac dolor. Mauris molestie diam nec eros accumsan pellentesque non quis felis. Aliquam erat volutpat. Sed sed est in lorem mollis iaculis et sed nunc. Integer vitae sapien ut arcu iaculis commodo. Donec molestie arcu sed augue malesuada tincidunt.\r\n\r\nIn lobortis justo at orci blandit, vitae commodo augue tristique. Aenean ornare condimentum ex, eu faucibus metus blandit eget. Curabitur condimentum egestas auctor. Cras tempus purus vel sem finibus, sed ullamcorper dui fringilla. Nulla sed tristique sapien, auctor ornare ante. Curabitur a efficitur sem, eu maximus est. Donec nisl justo, facilisis quis porta sed, ornare sed enim. Suspendisse vel semper nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam non euismod mauris, eget maximus ipsum. Fusce quis dolor vel est aliquet pharetra. Fusce vel erat a elit efficitur eleifend. Aenean ultricies mauris tincidunt nunc tincidunt aliquet. Aliquam et dignissim mi. In hac habitasse platea dictumst. Sed dictum consectetur bibendum.','2017-07-09 17:37:02','2017-07-09 17:37:02'),(2,'This is another page','Donec id lectus arcu. Sed elementum volutpat urna in eleifend. Vestibulum ac enim ac massa condimentum sollicitudin. Maecenas quis posuere purus. Aenean sed aliquam est. Mauris lacinia id nisl id interdum. Proin ut quam molestie, rhoncus diam sed, pharetra lorem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse pharetra fringilla enim eu vestibulum.\r\n\r\nDonec at ullamcorper nibh. Proin eget efficitur est. Curabitur ut nisl fermentum metus varius tincidunt. Donec quis elit id mi posuere ornare. In hac habitasse platea dictumst. Nulla maximus maximus enim, blandit aliquam mauris mollis porttitor. Sed id sem suscipit turpis viverra aliquam. Nullam tincidunt ac turpis non placerat. Quisque dapibus laoreet euismod. Maecenas eget tristique ex. Suspendisse lobortis tellus vel interdum vulputate. Phasellus ultricies venenatis lacus vitae condimentum. Nam cursus efficitur porta. Aliquam ac dignissim nunc.','2017-07-09 17:38:19','2017-07-09 17:38:19');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-09 17:40:44
