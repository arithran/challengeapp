<?php
App::uses('CommentsController', 'Controller');

/**
 * Class: CommentsControllerTest
 *
 * @author Arithran Thurairetnam 
 * @see ControllerTestCase
 */
class CommentsControllerTest extends ControllerTestCase {

	/**
	 * Fixtures
	 *
	 * @var array
	 */
	public $fixtures = array(
		'app.comment',
		'app.page'
	);

	/**
	 * testApiPage
	 *
	 * Test the comment API's GET request
	 *
	 * @author Arithran Thurairetnam
	 * @access public
	 * @return void
	 */
	public function testApiPage() {

		$result = $this->testAction('/api/comments/page/1.json', array('return' => 'vars'));
		$this->assertTrue($result['success']);
		$this->assertNotEmpty($result['data']);
	}

	/**
	 * testApiAdd
	 *
	 * Test adding a post using the API
	 *
	 * @author Arithran Thurairetnam
	 * @access public
	 * @return void
	 */
	public function testApiAdd() {
		$this->Comment = ClassRegistry::init('Comment');

		// Test before adding comment, Should be empty
		$data = $this->Comment->find('first', array(
			'conditions' => array(
				'Comment.name' => 'Foo Bar'
			)
		));
		$this->assertEmpty($data);

		// Add a new comment
		$data = array(
			'name' => 'Foo Bar',
			'text' => 'This post sucks',
			'page_id' => 1,
		);
		$this->testAction('/api/comments/add.json', array('data' => $data, 'method' => 'post'));

		// Check if comment is there
		$data = $this->Comment->find('first', array(
			'conditions' => array(
				'Comment.name' => 'Foo Bar'
			)
		));
		$this->assertNotEmpty($data);
	}

}
