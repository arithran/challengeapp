<?php
/**
 * Comment Fixture
 */
class CommentFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'page_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'parent_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'unsigned' => false),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'text' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '1',
			'page_id' => '1',
			'parent_id' => null,
			'name' => 'Bob',
			'text' => 'Nice Article!',
			'created' => '2017-07-09 17:37:20',
			'modified' => '2017-07-09 17:37:20'
		),
		array(
			'id' => '2',
			'page_id' => '1',
			'parent_id' => '1',
			'name' => 'Ari',
			'text' => 'Thanks Bob!',
			'created' => '2017-07-09 17:37:30',
			'modified' => '2017-07-09 17:37:30'
		),
		array(
			'id' => '3',
			'page_id' => '1',
			'parent_id' => null,
			'name' => 'Bill',
			'text' => 'It\'s not bad',
			'created' => '2017-07-09 17:37:51',
			'modified' => '2017-07-09 17:37:51'
		),
		array(
			'id' => '4',
			'page_id' => '2',
			'parent_id' => null,
			'name' => 'Bob',
			'text' => 'I like this article better than the first!',
			'created' => '2017-07-09 17:38:35',
			'modified' => '2017-07-09 17:38:35'
		),
	);

}
