<?php
/**
 * Page Fixture
 */
class PageFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'body' => array('type' => 'text', 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => '1',
			'name' => 'This is a test page',
			'body' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi at posuere dui. Donec magna purus, posuere vitae egestas id, eleifend at neque. Cras egestas, diam eu dictum volutpat, velit orci tincidunt lacus, sit amet pretium mi magna ac dolor. Mauris molestie diam nec eros accumsan pellentesque non quis felis. Aliquam erat volutpat. Sed sed est in lorem mollis iaculis et sed nunc. Integer vitae sapien ut arcu iaculis commodo. Donec molestie arcu sed augue malesuada tincidunt.

In lobortis justo at orci blandit, vitae commodo augue tristique. Aenean ornare condimentum ex, eu faucibus metus blandit eget. Curabitur condimentum egestas auctor. Cras tempus purus vel sem finibus, sed ullamcorper dui fringilla. Nulla sed tristique sapien, auctor ornare ante. Curabitur a efficitur sem, eu maximus est. Donec nisl justo, facilisis quis porta sed, ornare sed enim. Suspendisse vel semper nisl. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam non euismod mauris, eget maximus ipsum. Fusce quis dolor vel est aliquet pharetra. Fusce vel erat a elit efficitur eleifend. Aenean ultricies mauris tincidunt nunc tincidunt aliquet. Aliquam et dignissim mi. In hac habitasse platea dictumst. Sed dictum consectetur bibendum.',
			'created' => '2017-07-09 17:37:02',
			'modified' => '2017-07-09 17:37:02'
		),
		array(
			'id' => '2',
			'name' => 'This is another page',
			'body' => 'Donec id lectus arcu. Sed elementum volutpat urna in eleifend. Vestibulum ac enim ac massa condimentum sollicitudin. Maecenas quis posuere purus. Aenean sed aliquam est. Mauris lacinia id nisl id interdum. Proin ut quam molestie, rhoncus diam sed, pharetra lorem. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse pharetra fringilla enim eu vestibulum.

Donec at ullamcorper nibh. Proin eget efficitur est. Curabitur ut nisl fermentum metus varius tincidunt. Donec quis elit id mi posuere ornare. In hac habitasse platea dictumst. Nulla maximus maximus enim, blandit aliquam mauris mollis porttitor. Sed id sem suscipit turpis viverra aliquam. Nullam tincidunt ac turpis non placerat. Quisque dapibus laoreet euismod. Maecenas eget tristique ex. Suspendisse lobortis tellus vel interdum vulputate. Phasellus ultricies venenatis lacus vitae condimentum. Nam cursus efficitur porta. Aliquam ac dignissim nunc.',
			'created' => '2017-07-09 17:38:19',
			'modified' => '2017-07-09 17:38:19'
		),
	);

}
