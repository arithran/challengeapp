<?php
App::uses('AppController', 'Controller');
/**
 * Class: CommentsController
 *
 * @author Arithran Thurairetnam 
 *
 * @property Comment $Comment
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class CommentsController extends AppController {

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator', 'Session', 'Flash');

	/**
	 * index
	 *
	 * List all comments
	 *
	 * @author Arithran Thurairetnam
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->Comment->recursive = 0;
		$this->set('comments', $this->Paginator->paginate());
	}

	/**
	 * delete
	 *
	 * Action to delete comments
	 *
	 * @author Arithran Thurairetnam
	 * @throws NotFoundException
	 * @param int $id
	 * @return void
	 */
	public function delete($id = null) {
		$this->Comment->id = $id;
		if (!$this->Comment->exists()) {
			throw new NotFoundException(__('Invalid comment'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Comment->delete()) {
			$this->Flash->success(__('The comment has been deleted.'));
		} else {
			$this->Flash->error(__('The comment could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
	 * api_page
	 *
	 * API call that will list all the comments for a given page
	 *
	 * @author Arithran Thurairetnam
	 * @access public
	 * @param int $page_id
	 * @return JSON
	 */
	public function api_page($page_id = null) {

		// Check if the page exists
		$this->loadModel('Page');
		$this->Page->id = $page_id;
		if (!$this->Page->exists()) {

			return $this->set(array(
				'success' => false,
				'error' => 'Page doesn\'t exist',
				'_serialize' => array('success', 'error')
			));
		}

		// Find comments for the page
		$comments = $this->Comment->find('threaded', array(
			'conditions' => array('Comment.page_id' => $page_id),
			'contain' => array()
		));
		return $this->set(array(
			'success' => true,
			'data' => $comments,
			'_serialize' => array('success', 'data')
		));
	}

	/**
	 * api_add
	 *
	 * API to add a new comment
	 *
	 * @author Arithran Thurairetnam
	 * @access public
	 * @return JSON
	 */
	public function api_add() {
		if ($this->request->is('post')) {
			$this->Comment->create();
			if ($this->Comment->save($this->request->data)) {

				// Show success
				return $this->set(array(
					'success' => true,
					'_serialize' => array('success', 'error')
				));
			} else {

				// Find all the validation errors
				$validationErrors = array();
				foreach ($this->Comment->validationErrors as $field => $error) {
					$validationErrors[$field] = implode(',', $error);
				}

				// Show error
				return $this->set(array(
					'success' => false,
					'error' => $validationErrors,
					'_serialize' => array('success', 'error')
				));
			}
		}
		$pages = $this->Comment->Page->find('list');
		$parentComments = $this->Comment->ParentComment->find('list');
		$this->set(compact('pages', 'parentComments'));
	}
}
