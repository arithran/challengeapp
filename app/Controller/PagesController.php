<?php
App::uses('AppController', 'Controller');
/**
 * Class: PagesController
 *
 * @author Arithran Thurairetnam 
 * @property Page $Page
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class PagesController extends AppController {

	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('Paginator', 'Session', 'Flash');

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index() {
		$this->Page->recursive = 0;
		$this->set('pages', $this->Paginator->paginate());
	}

	/**
	 * view
	 *
	 * View a page
	 *
	 * @author Arithran Thurairetnam
	 * @throws NotFoundException
	 * @param int $id
	 * @return void
	 */
	public function view($id = null) {
		if (!$this->Page->exists($id)) {
			throw new NotFoundException(__('Invalid page'));
		}
		$options = array('conditions' => array('Page.' . $this->Page->primaryKey => $id));
		$this->set('page', $this->Page->find('first', $options));
	}

	/**
	 * add
	 *
	 * Add a new page
	 *
	 * @author Arithran Thurairetnam
	 * @access public
	 * @return void
	 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Page->create();
			if ($this->Page->save($this->request->data)) {
				$this->Flash->success(__('The page has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The page could not be saved. Please, try again.'));
			}
		}
	}

	/**
	 * edit
	 *
	 * Edit a page
	 *
	 * @author Arithran Thurairetnam
	 * @throws NotFoundException
	 * @param int $id
	 * @return void
	 */
	public function edit($id = null) {
		if (!$this->Page->exists($id)) {
			throw new NotFoundException(__('Invalid page'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Page->save($this->request->data)) {
				$this->Flash->success(__('The page has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The page could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Page.' . $this->Page->primaryKey => $id));
			$this->request->data = $this->Page->find('first', $options);
		}
	}

	/**
	 * delete
	 *
	 * Delete a page
	 *
	 * @author Arithran Thurairetnam
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null) {
		$this->Page->id = $id;
		if (!$this->Page->exists()) {
			throw new NotFoundException(__('Invalid page'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Page->delete()) {
			$this->Flash->success(__('The page has been deleted.'));
		} else {
			$this->Flash->error(__('The page could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	/**
	 * checkhealth
	 *
	 * Check if the app is setup properly.
	 * Nothing should be RED
	 *
	 * @author Arithran Thurairetnam
	 * @access public
	 * @return void
	 */
	public function checkhealth() {
		$this->layout = 'cakephp';
	}
}
