<?php
App::uses('Controller', 'Controller');
/**
 * Class: AppController
 *
 * Application level Controller
 *
 * @author Arithran Thurairetnam 
 * @property RequestHandlerComponent $RequestHandler
 * @see Controller
 */
class AppController extends Controller {
	/**
	 * Components
	 *
	 * @var array
	 */
	public $components = array('RequestHandler');
}
