<!DOCTYPE html>
<html lang="en" ng-app="challenge-app">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ChallengeApp</title>

	<?php 
    // Bootstrap core CSS
	echo $this->Html->css('bootstrap.min');

    // IE10 viewport hack for Surface/desktop Windows 8 bug
	echo $this->Html->css('ie10-viewport-bug-workaround');

    // Custom styles for this template
	echo $this->Html->css('starter-template');

    // Just for debugging purposes. Don't actually copy these 2 lines!
	echo $this->Html->script('ie-emulation-modes-warning');

	// Custom styles
	echo $this->Html->css('styles');

    // Bootstrap core JavaScript
	echo $this->Html->script('jquery.min');
	echo $this->Html->script('bootstrap.min');
	echo $this->Html->script('ie10-viewport-bug-workaround');


	// Load AngularJS Modules
	echo $this->Html->script('angular.min.js', array('defer'=> true));
	echo $this->Html->script('angular-resource.min.js', array('defer'=> true));
	echo $this->Html->script('angular-animate.min.js', array('defer'=> true));
	echo $this->Html->script('angular-route.min.js', array('defer'=> true));
	echo $this->Html->script('app.js', array('defer'=> true));

	// Print any scriptBlocks
	echo $this->fetch('script');
	?>

  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">TMBC ChallengeApp</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
			<li><?php echo $this->Html->link('Home', array('controller' => 'pages', 'action' => 'index')) ?></li>
			<li><?php echo $this->Html->link('CheckHealth', array('controller' => 'pages', 'action' => 'checkhealth')) ?></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">

      <div class="starter-template">
			<?php echo $this->Flash->render(); ?>
			<?php echo $this->fetch('content'); ?>
      </div>

		<?php echo $this->element('sql_dump'); ?>
    </div><!-- /.container -->
  </body>
</html>

