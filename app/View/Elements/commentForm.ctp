<?php if (isset($type) && $type == 'main'): ?>
<!-- Main comment form -->
<form class="form-horizontal">
	<div class="form-group">
		<input type="text" class="form-control" placeholder="Name" size="8" ng-model="newComment.name">
		<span class="text-danger">{{newComment.error_name}}</span>

	</div>
	<div class="form-group">
		<textarea class="form-control" rows="3" placeholder="Comment" ng-model="newComment.text"></textarea>
		<span class="text-danger">{{newComment.error_text}}</span>
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-default"  ng-click="save(newComment, true)">Post</button>
	</div>
</form>
<?php else: ?>
<!-- Reply to existing comment form -->
<form ng-show="comment.Comment.id == replyTo " class="form-inline">
	<div class="form-group">
		<input type="text" class="form-control" placeholder="Name" size="8" ng-model="replyToExistingComment.name">
	</div>
	<div class="form-group">
		<input type="text" class="form-control" placeholder="Comment" ng-model="replyToExistingComment.text">
	</div>
	<button type="submit" class="btn btn-default"  ng-click="save(replyToExistingComment, false)">Post</button>
	<button type="submit" class="btn btn-default"  ng-click="reset()">Cancel</button>
	<span class="text-danger">{{replyToExistingComment.error_name}}</span>
	<span class="text-danger">{{replyToExistingComment.error_text}}</span>
</form>
<?php endif; ?>
	
