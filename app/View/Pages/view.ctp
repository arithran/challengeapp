<div class="pages view">
	<h1><?php echo sprintf('#%d %s', $page['Page']['id'], $page['Page']['name'])?></h1>
	<p class="lead"><?php echo $page['Page']['body'] ?></p>
	<br/><hr/>

	<?php
	// Set the current page_id as a global JS var for AngularJS to load related comments
	$this->Html->scriptStart(array('inline' => false));
	echo sprintf("var page_id = '%s';", $page['Page']['id']);
	$this->Html->scriptEnd();
	?>
	<div class="comments text-left" ng-controller="CommentsCtrl" ng-cloak>
		<h3><?php echo __('Comments'); ?></h3>

		<div ng-show="!comments" class="" >
			<!-- Loading Icon -->
			<span>Loading Comments</span>
			<?php echo $this->element('loadingIcon') ?>
		</div>

		<div ng-show="comments.length == 0">
			<!-- This page has no comments -->
			<small>You have no recent comments</small>
		</div>

		<!-- Level 1 -->
		<div class="media" ng:repeat="comment in comments"> 
			<div class="media-left"> 
				<a href="#"><?php echo $this->element('avatar') ?></a> 
			</div>
			<div class="media-body"> 
				<h4 class="media-heading">{{comment.Comment.name}}</h4> 
				{{comment.Comment.text}}<br />{{comment.Comment.created}}
				<a href="" ng-click="setReplyTo(comment)">Reply</a> 
				<?php echo $this->element('commentForm') ?>

				<!-- Level 2 -->
				<div ng-show="comment.children" class="media" ng:repeat="comment in comment.children"> 
					<div class="media-left"> 
						<a href="#"><?php echo $this->element('avatar') ?></a> 
					</div>
					<div class="media-body"> 
						<h4 class="media-heading">{{comment.Comment.name}}</h4> 
						{{comment.Comment.text}}<br />{{comment.Comment.created}}
						<a href="" ng-click="setReplyTo(comment)">Reply</a>
						<?php echo $this->element('commentForm') ?>

						<!-- Level 3 -->
						<div ng-show="comment.children" class="media" ng:repeat="comment in comment.children"> 
							<div class="media-left"> 
								<a href="#"><?php echo $this->element('avatar') ?></a> 
							</div>
							<div class="media-body"> 
								<h4 class="media-heading">{{comment.Comment.name}}</h4> 
								{{comment.Comment.text}}<br />{{comment.Comment.created}}
							</div> 
						</div>
					</div> 
				</div>
			</div> 
		</div>
		<?php echo $this->element('commentForm', array('type' => 'main')) ?>


	</div>
</div>
