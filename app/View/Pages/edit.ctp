<div class="pages form text-left">
<?php
echo $this->Form->create('Page', array('class' => 'form-horizontal'));
echo $this->Form->input('id');
echo $this->Form->input('name', array('div' => 'form-group', 'class' => 'form-control'));
echo $this->Form->input('body', array('div' => 'form-group', 'class' => 'form-control'));
echo $this->Form->submit(__('Submit'), array('div' => '', 'class' => 'btn btn-default'));
echo $this->Form->end();
?>
</div>
