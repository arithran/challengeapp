/**
 * AngularJS App
 * -------------
 *
 * @version 1.0
 * @author Arithran Thurairetnam
 */
// AngularJS App Init
var app = angular.module("challenge-app",["ngResource", "ngRoute", "ngAnimate"]);

/**
 * Resources and Services
 * ----------------------
 */
app.factory("CommentResource", function($resource){
	// CommentResource API
	var CommentResource = $resource(
		'/api/comments/page/:id.json',
		{id: '@id'}
	);
	return CommentResource;
});

app.factory("CommentAddResource", function($resource){
	// CommentAddResource API
	var CommentAddResource = $resource(
		'/api/comments/add.json'
	);
	return CommentAddResource;
});

/**
 * Controllers
 * -----------
 */
app.controller("CommentsCtrl", function($scope, $interval, CommentResource, CommentAddResource){

	// Find and populate the comments
	function updateCommnets() {
		CommentResource.get({id: page_id}, function(results){
			if (results.success) {
				$scope.comments = results.data;
			}
		});
	}
	updateCommnets();


	// Setup empty skeletons for new comments
	$scope.reset = function() {
		$scope.replyTo = null;
		$scope.newComment = {name: '', text: ''};
		$scope.replyToExistingComment = {name: '', text: ''};
	}
	$scope.reset();

	// Select who to reply to when replying to an existing comment
	$scope.setReplyTo = function(comment) {
		$scope.reset();
		$scope.replyTo = comment.Comment.id;
	};

	// Save comment
	$scope.save = function(comment, isNewComment) {
		comment.page_id = page_id;
		if (!isNewComment) {
			comment.parent_id = $scope.replyTo;
		}

		CommentAddResource.save(comment, function(result) {
			if (result.success == true) {
				updateCommnets();
				$scope.reset();
			} else {
				if(typeof result.error.name !== "undefined") comment.error_name = result.error.name;
				if(typeof result.error.text !== "undefined") comment.error_text = result.error.text;
			}
		})
	};
})

