<?php
App::uses('Model', 'Model');
/**
 * Class: AppModel
 *
 * Application model for CakePHP.
 *
 * @author Arithran Thurairetnam 
 * @property ContainableBehavior $Containable
 * @see Model
 */
class AppModel extends Model {

	/**
	 * actsAs
	 * Attach Containable behaviour
	 */
	public $actsAs = array('Containable');
}
