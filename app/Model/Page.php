<?php
App::uses('AppModel', 'Model');
/**
 * Class: Page
 *
 * @author Arithran Thurairetnam 
 * @property Comment $Comment
 */
class Page extends AppModel {

	/**
	 * Display field
	 *
	 * @var string
	 */
	public $displayField = 'name';

	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'name' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Page name required',
			),
		),
		'body' => array(
			'notBlank' => array(
				'rule' => array('notBlank'),
				'message' => 'Page body cannot be empty',
			),
		),
	);


	/**
	 * hasMany associations
	 *
	 * @var array
	 */
	public $hasMany = array(
		'Comment' => array(
			'className' => 'Comment',
			'foreignKey' => 'page_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
}
