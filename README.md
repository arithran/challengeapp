# README #
This README  will show you the steps required to get this Challenge App up and running.

I am writing this guide with the intention of installing it on a Linux OS.
However, the same should apply to any Unix based systems like MAC OS.

I chose the latest version CakePHP 2 for this project instead of CakePHP 3 because the 
setup is much easier for this purpose with fewer dependencies and will work on most OS 
configurations. Both CakePHP 2 & 3 are actively maintained, with the ORM being the main difference.

NOTE: CakePHP already protects you against SQL Injection and against any Cross Site Scripting Attacks,
so there was no need to implement any extra code.

### How do I get set up? ###
Summary: 
I will show you how to get the app up and running on the following URL http://local.challengeapp.com. 


1) Get the files:

Download the git repository from https://bitbucket.org/arithran/challengeapp

```
git clone https://bitbucket.org/arithran/challengeapp.git
```

2) Setup Webserver (Apache):

Edit your hosts file (```vi /etc/hosts```) and add the following as a new line

```
127.0.0.1		local.challengeapp.com
```

Edit your virtual hosts file and add the following apache config (You may need
to find out where your virtual hosts are stored).

Please, change the DocumentRoot and Directory ```/home/ari/workspace/challengeapp/app/webroot```
to match your local setup 

```
<VirtualHost *:80>
        DocumentRoot /home/ari/workspace/challengeapp/app/webroot
        ServerName local.challengeapp.com:80
        ServerAlias local.challengeapp.com:80
        <Directory /home/ari/workspace/challengeapp/app/webroot>
                Options FollowSymLinks
                AllowOverride all
                php_flag display_errors On
                Require all granted
        </Directory>
</VirtualHost>
```

Now restart apache

3) Database Setup:

A copy of CakePHP’s database configuration file is found in ```/app/Config/database.php.default```
Make a copy of this file in the same directory, but name it ```database.php```

You should note that there are two database configurations inside database.php. 
```$default``` is used when the app runs as normal and ```$test``` when running
unit tests. 

- NOTE: ```$test``` is always erased and populated with fixture data when unit
tests are run so make sure you have separate databases for $default and $test.

You will have to update the 'host', 'login', 'password' and 'database' values for $default and $test.


Cake has an awesome schema migrations tool but to save time, source your $default database
using the file found in ```/app/challenge_app.sql``` (You don't have to do this for $test config)

Example:
```
mysql -uadmin -p -h challenge_app < challenge_app.sql
```

4) Finished:

- View the site on http://local.challengeapp.com
- View a sample page with comments on http://local.challengeapp.com/pages/view/1


### Troubleshoot ###
The app should look like this screenshot.
https://www.dropbox.com/s/stbfymuaifducrx/ChallengeApp.png?dl=0

1) Visit the following page
http://local.challengeapp.com/checkhealth

Everything should be green except for DebugKit

2) I've written a clear_cache script that should clear any hard file cache.
change directory to /app and run the following command.
```
../clear_cache.sh
```

3) Still having trouble? 

Contact Arithran Thurairetnam <arithran@gmail.com>


### What files should I look at? ###
If you are unfamiliar with CakePHP, I would suggest looking at the files in 
the following directories.

1) Most of the business and controller logic

- app/Model/*
- app/Controller/*

2) View Logic

- app/View/Comments/*
- app/View/Pages/*
- app/View/Elements/*

3) JavaScript Client App using AngularJS

- app/webroot/js/app.js

4) Unit test file

- app/Test/Case/Controller/CommentsControllerTest.php


### How does it work? ###
The page view is rendered by PHP but the comments section is asynchronous and
is rendered using AngularJS via a JSON REST API. Everything is stored in a MySQL
database.

This URL will accept a GET request and will list all the comments for the 
page with the id of 1
http://local.challengeapp.com/api/comments/page/1.json

This URL will accept a POST request and will add new comments for the current page
http://local.challengeapp.com/api/comments/add.json

### How do I run unit tests? ###
Run the following in a terminal from the /app directory

```
./Console/cake test app Controller/CommentsController
```
